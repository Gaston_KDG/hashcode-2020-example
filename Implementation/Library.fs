﻿namespace Hashcode.Implementation

open Hashcode.IO.IOWrapper
open Models

open Parsing

module Processing =
    let private stat (data:processingFormat) =
        let fillFactor =  ( double data.currentSlices/ double data.maxSlices)*double 100 
        printfn "Fillfactor %.12f%%" fillFactor
        printfn "Score %i" data.currentSlices
        data
    let private toOutput (data:processingFormat):outputFormat =
        outputFormat(data.pizzaTypesOrdered,data.chosenTypes |> Set.map (fun p -> p.Index))
        
    let rec private fillQuota (data:processingFormat):processingFormat =
        //find biggest slice that fits
        let biggestPizza =
            data |> Utils.biggestFittingPizza
        //apply the slice
        let newWorkingData: processingFormat =
            { maxSlices = data.maxSlices
              currentSlices = data.currentSlices+biggestPizza.Slices
              pizzaTypesOrdered = data.pizzaTypesOrdered+1
              chosenTypes = data.chosenTypes.Add biggestPizza
              remainingTypes = data.remainingTypes.Remove biggestPizza
              pizzaTypes = data.pizzaTypes }
        //recurse
        if Utils.canFitAnotherPizza(newWorkingData)
        then
            fillQuota newWorkingData
        else
            newWorkingData
    
    let private processInput (input: inputFormat): outputFormat =
        let workingData: processingFormat =
            { maxSlices = input.MaxSlices
              currentSlices = 0
              pizzaTypesOrdered = 0
              chosenTypes = Set.empty
              remainingTypes = Set.ofSeq (input.PizzaTypes |> Array.mapi (fun i p -> Pizza(i,p)))
              pizzaTypes = input.PizzaTypes }
        
        workingData |> fillQuota |> stat |> toOutput

    let handle (inputPath: string, outputPath: string) =
        let stopWatch = System.Diagnostics.Stopwatch.StartNew()
        let output =
            inputPath
            |> readFile
            |> parseInput
            |> processInput
            |> serializeOutput
        writeLines (outputPath, Array.ofSeq output) 
        stopWatch.Stop()
        printfn "Execution time: %fms\n" stopWatch.Elapsed.TotalMilliseconds
