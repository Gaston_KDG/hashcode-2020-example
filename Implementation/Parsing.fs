namespace Hashcode.Implementation
open System
open System.Collections.Generic
open Models

module Parsing =
    let parseInput (lines:IEnumerable<string>):inputFormat =
        let arrLines = Array.ofSeq(lines)
        let firstline = arrLines.[0].Split(' ') |> Array.map (fun s -> Int32.Parse s)
        let pizzas = arrLines.[1].Split(' ') |> Array.map (fun s -> (Int32.Parse s))
        
        let input = inputFormat(firstline.[0], firstline.[1], pizzas)
        input
    
    let serializeOutput (out:outputFormat) =
        let line1 = sprintf "%A" out.PizzaTypesOrdered
        let line2 = String.Join(' ', out.PizzaTypes)
        
        seq [line1; line2]