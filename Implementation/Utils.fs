namespace Hashcode.Implementation

open Models

module Utils =
    let biggestFittingPizza (data: processingFormat) =
        let toFill = data.maxSlices - data.currentSlices
        data.remainingTypes
        |> Set.filter (fun p -> p.Slices <= toFill)
        |> Set.maxElement

    let canFitAnotherPizza (data: processingFormat) =
        let toFill = data.maxSlices - data.currentSlices
        data.remainingTypes
        |> Set.exists (fun p -> p.Slices <= toFill)
