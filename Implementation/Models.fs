namespace Hashcode.Implementation

module Models =

    type pizzaType = int

    type inputFormat =
        struct
            val MaxSlices: int
            val NumPizzaTypes: int
            val PizzaTypes: pizzaType []
            new(maxSlices: int, numPizzaTypes: int, pizzaTypes: pizzaType []) =
                { MaxSlices = maxSlices
                  NumPizzaTypes = numPizzaTypes
                  PizzaTypes = pizzaTypes }
        end

    type outputFormat =
        struct
            val PizzaTypesOrdered: int
            val PizzaTypes: Set<int>
            new(pizzaTypesOrdered: int, pizzaTypes: Set<pizzaType>) =
                { PizzaTypesOrdered = pizzaTypesOrdered
                  PizzaTypes = pizzaTypes }
        end

    type Pizza =
        struct
            val Index: int
            val Slices: int
            new(index: int, slices:int) =
                { Index=index
                  Slices=slices }
        end

    type processingFormat =
        { maxSlices: int
          currentSlices: int
          pizzaTypesOrdered: int
          chosenTypes: Set<Pizza>
          remainingTypes: Set<Pizza>
          pizzaTypes: pizzaType [] }
