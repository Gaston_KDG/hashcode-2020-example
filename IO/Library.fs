﻿namespace Hashcode.IO

open System.IO

module IOWrapper =
    let readFile filePath = System.IO.File.ReadLines(filePath);
    let writeLines (filePath:string, lines) =
        printfn "Writing to %A" filePath
        Directory.CreateDirectory(Path.GetDirectoryName(filePath)) |> ignore
        System.IO.File.WriteAllLines(filePath,lines);