﻿// Learn more about F# at http://fsharp.org

open System
open System.Collections
open System.Collections.Generic
open CommandLine
open Hashcode.Implementation
open Processing

type options =
    { [<Value(0, MetaName = "inputPath", Required = true, HelpText = "Input file path")>]
      inputPath: string
      [<Value(1, MetaName = "outputPath", Required = true, HelpText = "Output file path")>]
      outputPath: string }

let run (options: options) =
    printfn "processing %A" options.inputPath

    handle (options.inputPath, options.outputPath)
    0

let fail (errors: IEnumerable<Error>) =
    eprintfn "%A" errors
    1

[<EntryPoint>]
let main argv =
    let result = CommandLine.Parser.Default.ParseArguments<options>(argv)
    match result with
    | :? (Parsed<options>) as parsed -> run (parsed.Value)
    | :? (NotParsed<options>) as notParsed -> fail notParsed.Errors
    | _ -> fail []
